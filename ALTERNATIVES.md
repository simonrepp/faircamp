<!--
    SPDX-FileCopyrightText: 2024 Simon Repp
    SPDX-License-Identifier: CC0-1.0
-->

# Alternatives To Faircamp

- **bandcrash** [fluffy.itch.io/bandcrash](https://fluffy.itch.io/bandcrash)<br>
  «Bandcamp-style batch encoder and web player for independent musicians.»
- **blamscamp** [suricrasia.online/blamscamp/](https://suricrasia.online/blamscamp/)<br>
  «Create a bandcamp-style audio player for selling albums on itch.io.»
- **CD-R 700mb** [github.com/thebaer/cdr](https://github.com/thebaer/cdr)<br>
  «Static site generator for makingweb mixtapes in 2022.»
- **Funkwhale** [funkwhale.audio/](https://funkwhale.audio/)<br>
  «Funkwhale is a community-driven project that lets you listen and share music and audio within a decentralized, open network.»
- **jam.coop** [jam.coop/](https://jam.coop/)<br>
  «We're a co-operative music platform, 100% run by and for the benefit of musicians, fans and the people who make it.»
- **Mirlo** [mirlo.space/](https://mirlo.space/)<br>
  «Directly support musicians. Buy their music. Collectively owned and managed.»
- **Rauversion** [github.com/rauversion/rauversion-phx](https://github.com/rauversion/rauversion-phx)<br>
  «Rauversion is an open source music sharing platform.»
- **Resonate** [resonate.coop/](https://resonate.coop/)<br>
  «Resonate is the first community-owned music streaming service.»
- **Scritch** [torcado.itch.io/scritch-editor/](https://torcado.itch.io/scritch-editor/)<br>
  «Scritch is a media player + customization tool, spawned out of an interest for music artists to upload their albums to itch.io.»
