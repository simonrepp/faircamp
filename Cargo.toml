# SPDX-FileCopyrightText: 2021-2025 Simon Repp
# SPDX-License-Identifier: AGPL-3.0-or-later

[package]
authors = ["Simon Repp"]
edition = "2021"
name = "faircamp"
publish = false
# rust-version here applies to --features libvips, with --features image it is 1.79.0
rust-version = "1.76.0"
version = "1.2.0"

[dependencies]
actix-files = "0.6.6"
actix-web = "4.9.0"
alac = "0.5.0"
base64 = "0.22.1"
bincode = "1.3.3"
chrono = { features = ["serde"], version = "0.4.39" }
clap = { features = ["derive"], version = "4.5.28" }
claxon = "0.4.3"
enolib = { git = "https://codeberg.org/simonrepp/enolib-rs", tag = "0.5.0" }
hound = "3.5.1"
id3 = "1.16.0"
image = { optional = true, version = "0.25.5" }
indoc = "2.0.5"
iso_currency = "0.5.1"
lewton = "0.10.2"
# We intentionally pin and keep libvips at an old version because
# newer versions significantly limit the range of systems on which
# we can build faircamp with libvips support (due to higher system
# libvips version requirements).
libvips = { optional = true, version = "=1.5.1" }
metaflac = "0.2.8"
mp4parse = "0.17.0"
nanoid = "0.4.0"
ogg = "0.9.2"
opus = "0.3.0"
opus_headers = "0.1.2"
# We intentionally pin and keep pacmog at an old version because
# newer versions significantly raise the minimum supported rust version.
pacmog = "=0.4.1"
pulldown-cmark = { default-features = false, features = ["html", "simd"], version = "0.12.2" }
rmp3 = { features = ["float", "simd"], version = "0.3.1" }
sanitize-filename = "0.6.0"
seahash = "4.1.0"
serde = "1.0.217"
serde_derive = "1.0.217"
slug = "0.1.6"
tokio = { features = ["macros", "rt-multi-thread"], version = "1.43.0" }
translations = { path = "translations" }
url = "2.5.4"
urlencoding = "2.1.3"
webbrowser = "1.0.3"
zip = { features = ["deflate"], version = "2.2.2" }

[features]
image = ["dep:image"]
libvips = ["dep:libvips"]
