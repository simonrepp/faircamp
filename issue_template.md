If you have a _feature request_ please see this thread: https://codeberg.org/simonrepp/faircamp/issues/90

For anything else you're invited to remove this text and go ahead in submitting your issue.

Thank you!
