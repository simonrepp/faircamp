<!--
    SPDX-FileCopyrightText: 2023-2025 Simon Repp
    SPDX-License-Identifier: CC0-1.0
-->

# Credits

In a perfect world, the list below would include everyone that ever spread the
word about faircamp, built sites, helped friends, and a thousand other things
that made a difference. In this real world, overwhelmingly complex and often
rushed, this is still an attempt to give credit to an extent that is
pragmatically feasible.

If you're among the countless people that contributed in some small or not so
small way, even if you are not listed here, thank you so much as well! If you
notice someone missing that absolutely should be on the list, or if you would
rather be listed differently, or not listed at all, please do let me know!

The list is in no particular order (thus alphabetical).

Thanks everyone for your support.

[ailepet](https://codeberg.org/ailepet)<br>
[Amamanita Axaxaxanax Glass Seer](https://aags.screaming.beauty)<br>
[Alexander Cobleigh](https://cblgh.org)<br>
[Andy Berdan](https://codeberg.org/berdandy)<br>
[atomkarinca](https://fe.disroot.org/users/atomkarinca)<br>
[audiokontor](https://audiokontor.net/)<br>
[AxWax](https://axwax.eu/)<br>
[bran(...)pos](https://www.soundcrack.net/)<br>
[Christian Pacaud](https://www.christianpacaud.com/)<br>
[controlfreak](https://control.org/)<br>
[Crocuta](https://crocutaband.org/)<br>
[DaforLynx](https://dafor.link/)<br>
Damian Szetela<br>
[Dave Lane](https://davelane.nz)<br>
[Dave Riedstra](https://daveriedstra.com/)<br>
[Deborah Pickett](https://icemoonprison.com)<br>
[Dec](https://spacehey.com/dec23k/)<br>
[Default Media Transmitter](https://defaultmediatransmitter.com/)<br>
[Denys Nykula](https://libre.net.ua/)<br>
[Devin Canterberry](https://canterberry.cc/)<br>
[DURAD](https://durad.xyz)<br>
[Edgar Aichinger](https://build.opensuse.org/users/edogawa)<br>
Élie Khalil<br>
[Elx Cat](https://mastodont.cat/@elx)<br>
[Emanuel Pesendorfer](https://emanuel-pesendorfer.at/)<br>
[Esi Jóhannes G.](https://esi.is/)<br>
[Filip](https://autom.zone/)<br>
[Flavigula](https://flavigula.net/)<br>
[Florian Antoine](http://oooooooooo.net/)<br>
[Francesco Gazzetta](https://codeberg.org/fgaz)<br>
[fREW Schmidt](https://blog.afoolishmanifesto.com/)<br>
[Friedrich](https://codeberg.org/Friedrich)<br>
[Fugue State Audio](https://www.fstateaudio.com/)<br>
[GoodTech](https://codeberg.org/GoodTech)<br>
[Harald Eilertsen](https://volse.net/)<br>
[James](https://strangetextures.com)<br>
[James Fenn](https://jfenn.me/)<br>
[JΛПIПΛIПFΛ](https://janinainfa.surge.sh/)<br>
[jd42](https://codeberg.org/jd42)<br>
[keef](https://key13.uk)<br>
[kel](https://mastodon.online/@kel)<br>
[Kristoffer Lislegaard](https://lisleg.com/)<br>
[Léon van Kammen](https://2wa.isvery.ninja)<br>
[Lime Bar](https://mastodon.social/@limebar)<br>
[lnicola](https://dend.ro/)<br>
[Lorenzo Miniero](https://fosstodon.org/@lminiero)<br>
[Lorenzo's Music](https://www.lorenzosmusic.com)<br>
[Mac Chaffee](https://www.macchaffee.com)<br>
[Mahlon E. Smith](https://martini.nu/)<br>
[marqh](https://codeberg.org/marqh)<br>
[Maxwell Volume](https://maxvolu.me/)<br>
[Meljoann](https://www.meljoann.com/)<br>
[Miró Allard](https://www.miroallard.com/)<br>
[naskya](https://naskya.net/)<br>
[ncc1988](https://codeberg.org/ncc1988)<br>
[ndr_brt](https://codeberg.org/ndr_brt)<br>
[n3wjack](https://n3wjack.net/)<br>
[Niklas Reppel](https://parkellipsen.de)<br>
[n00q](https://codeberg.org/n00q)<br>
[OniriCorpe](https://oniricorpe.eu)<br>
[Patrik Wallström](https://mastodon.social/@pawal)<br>
[Porkepix](https://github.com/Porkepix)<br>
[protman](https://protman.com/)<br>
[rakoo](https://codeberg.org/rakoo)<br>
[Rotfarm](https://eldritch.cafe/@rotfarm)<br>
[Samæ](https://samae.demer.se/)<br>
[Sandro Santilli](https://strk.kbt.io/)<br>
[session](https://codeberg.org/session)<br>
[sknob](https://sknob.fr/)<br>
[Sunny](https://sny.sh)<br>
[themissingcow](https://codeberg.org/themissingcow)<br>
[thirdwheel](https://codeberg.org/thirdwheel)<br>
[thurti](https://t3000.uber.space)<br>
[Tommaso Croce](https://mastodon.uno/@toctoc)<br>
[Torsten Torsten](https://torstentorsten.de)<br>
[Travis Briggs](https://travisbriggs.com/)<br>
[Vac](https://crumb.lt/Vac)<br>
[Venya](https://venya.soundslike.pro/)<br>
[Yonder](https://spacey.space/@yonder)<br>
[Wesley Sinks](https://linuxcreative.com)<br>
[wileyfoxyx](https://codeberg.org/wileyfoxyx)<br>
[Wolfgang Dorninger](https://dorninger.servus.at)<br>
[zzkt](https://fo.am/people/nik)
