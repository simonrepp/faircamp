<!--
    SPDX-FileCopyrightText: 2023-2025 Simon Repp
    SPDX-License-Identifier: CC0-1.0
-->

# Faircamp Manual

The manual is organized into *topics*, *examples* and *reference* pages.

## Topics

For an introduction go through the topics in order. These show how faircamp
works in general and swiftly guide you to your first faircamp site.

## Examples

These each explain a fictional faircamp catalog - how the file layout is
structured, what metadata from audio files it uses, and how the manifests are
placed and used to customize the site for each respective usecase. This
teaches by example, but only shows *some* of the features in faircamp, so
definitely check out the reference to get the full picture.

## Reference

A faircamp site is tweaked, configured, extended, themed, etc. through
so called manifests. There are four different types of manifest files,
clearly identified by their respective filename ([artist.eno](artists-artist-eno.html),
[catalog.eno](catalog-catalog-eno.html), [release.eno](releases-release-eno.html),
[track.eno](tracks-track-eno.html)). The reference provides a detailed
list of all available options for each type of manifest.

## More resources

See the [website](https://simonrepp.com/faircamp) for further community
resources, a feature overview, screenshots, and a list of faircamp sites to
explore.

See the [repository](https://codeberg.org/simonrepp/faircamp) for the source code.
