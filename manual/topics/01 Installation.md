<!--
    SPDX-FileCopyrightText: 2023-2025 Simon Repp
    SPDX-License-Identifier: CC0-1.0
-->

# Installation

## Windows

Download and run the [Windows Installer](https://simonrepp.com/faircamp/packages/faircamp-1.2.0-windows-x64.exe) (64-bit)

Alternatively, you can also run faircamp non-natively via [Docker](https://github.com/n3wjack/faircamp-docker) or via
[WSL](https://www.meljoann.com/posts/faircamp/), or build it yourself using the
general build instructions in [Building from source](https://codeberg.org/simonrepp/faircamp/src/branch/main/BUILD.md)

## macOS

1. Install [Homebrew](https://brew.sh/)
2. Run `brew install faircamp` to install the [faircamp formula](https://formulae.brew.sh/formula/faircamp)

## Linux

**Arch Linux, Manjaro**

Install the [faircamp AUR package](https://aur.archlinux.org/packages/faircamp)

**Debian 12, elementary OS 8, Linux Mint 22, Ubuntu 23.04 - 24.10**

Install the [latest debian 12 package](https://simonrepp.com/faircamp/packages/faircamp_1.2.0-1+deb12_amd64.deb) (amd64)

**Debian 11, elementary OS 7, Linux Mint 21, Ubuntu 22.04 LTS - 22.10**

Install the [latest debian 11 package](https://simonrepp.com/faircamp/packages/faircamp_1.2.0-1+deb11_amd64.deb) (amd64)

**Fedora, CentOS, RHEL**

See copy/paste instructions in [Building from source](https://codeberg.org/simonrepp/faircamp/src/branch/main/BUILD.md)

**Nix/NixOS**

Install the [faircamp package](https://search.nixos.org/packages?show=faircamp&type=packages&query=faircamp)

**openSUSE**

Install the [faircamp package](https://software.opensuse.org/download.html?project=multimedia%3Aproaudio&package=faircamp)

## FreeBSD

See copy/paste instructions in [Building from source](https://codeberg.org/simonrepp/faircamp/src/branch/main/BUILD.md)

## YunoHost

Install the [faircamp](https://apps.yunohost.org/app/faircamp) app

## Other platforms

See general build instructions in [Building from source](https://codeberg.org/simonrepp/faircamp/src/branch/main/BUILD.md)
