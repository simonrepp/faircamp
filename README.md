<!--
    SPDX-FileCopyrightText: 2021-2025 Simon Repp
    SPDX-License-Identifier: CC0-1.0
-->

# Faircamp

**A static site generator for audio producers**

Point Faircamp to a folder hierarchy containing your audio files. Within
minutes, Faircamp builds a complete website presenting your work. The
resulting site requires no database, no programming, no maintenance, and is
compatible with virtually every webhost on this planet.

Latest Release: **`1.2`** – Visit the [website](https://simonrepp.com/faircamp) to get started.

<img src="https://simonrepp.com/faircamp/readme.jpg?2" alt="Screenshot of a pink/violet-colored website presenting an album called 'Valedictions EP' by artist 'Turnus'. The most prominent features are big buttons labelled 'Pause', 'Download' and 'Extra info', a short synopsis text, smaller buttons labelled 'Copy link' and 'M3U Playlist', a list of tracks decorated with delicate lines resembling waveforms and at the very bottom a YouTube-like control bar for playing/pausing, going to the next track, setting the volume, or seeking in the track. One track is playing, it's name is 'Distances'."/>

## Documentation

The [website](https://simonrepp.com/faircamp) is the central information hub:

- Feature overview
- Tutorials for Linux, macOS and Windows
- Community resources
- Showcase of sites using faircamp
- Links to further resources

The [manual](https://simonrepp.com/faircamp/manual) covers specific aspects:

- Installation
- Reference
- Usage examples
- Information on various topics

## Faircamp 1.0 and beyond

Major developments on Faircamp (Faircamp 1.0 in 2024, and also beyond it in
2025) are being financed by the European Commission's [Next Generation Internet](https://www.ngi.eu/)
programme through the [NGI0 Entrust](https://nlnet.nl/entrust/) fund established
by [NLnet](https://nlnet.nl/) (see the [project grant website](https://nlnet.nl/project/Faircamp/)
for more details). On behalf of myself and all the people and communities benefitting
from this work, I'd like to express my gratefulness for being given this
amazing opportunity and incredible support, thank you so much!

## Development

Building faircamp from source is pretty straight-forward, see the instructions
in [`BUILD.md`](https://codeberg.org/simonrepp/faircamp/src/branch/main/BUILD.md).

Building the manual is also trivial: It's a subcrate located in the
[`manual`](https://codeberg.org/simonrepp/faircamp/src/branch/main/manual) folder in this repository, just follow its `README.md`.

## Licensing

Faircamp is licensed under the [AGPL-3.0-or-later](https://spdx.org/licenses/AGPL-3.0-or-later.html).

Documentation is licensed under the [CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/).

Builds generated with faircamp re-distribute the [Barlow](https://tribby.com/fonts/barlow/) font,
licensed under the [OFL-1.1](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

The faircamp manual re-distributes the [Fira Mono](https://github.com/mozilla/Fira) and
[Titillium Web](http://nta.accademiadiurbino.it/titillium/) fonts, licensed under the
[OFL-1.1](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).
